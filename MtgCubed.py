from PySide import QtGui
from PySide import QtCore
import json
import codecs
import collections
from urllib.request import urlopen
import sys

cardsDict = {}
sections = collections.OrderedDict([("White", 0), ("Blue", 1), ("Black", 2), ("Red", 3), ("Green", 4),
    ("Multicolor", 5), ("Artifact", 6), ("Land", 7)])
properties = [("name", "Name"), ("names", "Names"), ("manaCost", "Mana Cost"), ("cmc", "CMC"),
    ("colors", "Colors"), ("type", "Type"), ("supertypes", "Supertypes"), ("types", "Types"),
    ("subtypes", "Subtypes"), ("rarity", "Rarity"), ("text", "Text"), ("flavor", "Flavor"),
    ("artist", "Artist"), ("number", "Number"), ("power", "Power"), ("toughness", "Toughness"),
    ("loyalty", "Loyalty"), ("layout", "Layout"), ("multiverseid", "Multiverse ID"),
    ("variations", "Variations"), ("imageName", "Image Name"), ("watermark", "Watermark"),
    ("border", "Border")]

def loadCards():
    """Load the master card list."""
    global cardsDict
    with codecs.open("data\AllSets.json", encoding="utf-8") as readfile:
        cardsDict = json.load(readfile, object_pairs_hook=collections.OrderedDict)

def sortCard(card):
    """Sort the card properties."""
    global properties
    sortedCard = []
    for key, printed in properties:
        if key in card:
            sortedCard.append((printed, card[key]))
    return sortedCard

def section(colors, types):
    """Return the section for the given colors."""
    global sections
    if len(colors) == 0:
        if "Land" in types:
            return sections["Land"], "Land"
        else:
            return sections["Artifact"], "Artifact"
    elif len(colors) > 1:
        return sections["Multicolor"], "Multicolor"
    else:
        return sections[colors[0]], colors[0]

def getCardImage(cardID):
        """Return the image for the corresponding multiverse ID."""
        url = "http://gatherer.wizards.com/Handlers/Image.ashx?type=card&multiverseid={id}".format(id = cardID)

        with urlopen(url) as req:
            image = req.read()

        return image

class MainWindow(QtGui.QMainWindow):

    """Manage the main Qt window."""

    def __init__(self):
        """Initialize the main window."""
        super(MainWindow, self).__init__()

        self.centerWidget = CenterWidget(self)
        self.setCentralWidget(self.centerWidget)
        self.fileName = None

        self.createActions()
        self.createMenus()

        self.initWindow()

    def initWindow(self):
        """Set the properties of the Qt window."""
        self.resize(800, 600)
        self.setWindowTitle("MTG³")
        self.setWindowIcon(QtGui.QIcon("images\\icon.ico"))

        # center the window
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def createActions(self):
        """Create menu actions."""
        self.newAct = QtGui.QAction(QtGui.QIcon("images\\icons\\page.png"), "&New", self,
                                     shortcut=QtGui.QKeySequence.New,
                                     statusTip="Start a new deck",
                                     triggered=self.newFile)
        self.openAct = QtGui.QAction(QtGui.QIcon("images\\icons\\folder_page.png"), "&Open...", self,
                                     shortcut=QtGui.QKeySequence.Open,
                                     statusTip="Open an existing deck",
                                     triggered=self.openFile)
        self.saveAct = QtGui.QAction(QtGui.QIcon("images\\icons\\page_save.png"), "&Save", self,
                                     shortcut=QtGui.QKeySequence.Save,
                                     statusTip="Save the deck",
                                     triggered=self.saveFile)
        self.saveAsAct = QtGui.QAction("Save &As...", self,
                                     shortcut=QtGui.QKeySequence.SaveAs,
                                     statusTip="Save the deck in a new location",
                                     triggered=self.saveFileAs)
        self.exitAct = QtGui.QAction(QtGui.QIcon("images\\icons\\door_in.png"), "E&xit", self,
                                     shortcut=QtGui.QKeySequence.Quit,
                                     statusTip="Exit the application",
                                     triggered=self.close)
        self.undoAct = QtGui.QAction(QtGui.QIcon("images\\icons\\arrow_undo.png"), "&Undo", self,
                                     shortcut=QtGui.QKeySequence.Undo,
                                     statusTip="Undo the last command",
                                     triggered=self.centerWidget.deckTree.undoStack.undo)
        self.redoAct = QtGui.QAction(QtGui.QIcon("images\\icons\\arrow_redo.png"), "&Redo", self,
                                     shortcut=QtGui.QKeySequence.Redo,
                                     statusTip="Redo the last command",
                                     triggered=self.centerWidget.deckTree.undoStack.redo)
        self.deleteAct = QtGui.QAction(QtGui.QIcon("images\\icons\\delete.png"), "De&lete", self,
                                     shortcut=QtGui.QKeySequence.Delete,
                                     statusTip="Delete the selected card",
                                     triggered=self.centerWidget.deckTree.deleteCard)
        self.aboutAct = QtGui.QAction(QtGui.QIcon("images\\icons\\help.png"), "&About", self,
                                     statusTip="Delete the selected card",
                                     triggered=self.about)

    def createMenus(self):
        """Create menus."""
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.newAct)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.saveAct)
        self.fileMenu.addAction(self.saveAsAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)
        self.editMenu = self.menuBar().addMenu("&Edit")
        self.editMenu.addAction(self.undoAct)
        self.editMenu.addAction(self.redoAct)
        self.editMenu.addSeparator()
        self.editMenu.addAction(self.deleteAct)
        self.helpMenu = self.menuBar().addMenu("&Help")
        self.helpMenu.addAction(self.aboutAct)

    def promptSave(self):
        """Prompt user to save."""
        saveCanceled = False
        ret = None
        if self.centerWidget.deckTree.undoStack.isClean() == False:
            ret = QtGui.QMessageBox.warning(self, "MTG³",
                                            "Do you want to save changes?",
                                            QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel,
                                            QtGui.QMessageBox.Save)
            if ret == QtGui.QMessageBox.Save:
                saveCanceled = self.saveFile()
        return ret, saveCanceled

    def newFile(self):
        """Create a new file."""
        ret, saveCanceled = self.promptSave()
        if ret != QtGui.QMessageBox.Cancel and saveCanceled != True:
            self.fileName = None
            self.centerWidget.deckTree.clear()
            self.centerWidget.deckTree.selectionModel.currentRowChanged.connect(self.centerWidget.cardSelectionChanged)

    def openFile(self):
        """Open an existing file."""
        ret, saveCanceled = self.promptSave()
        if ret != QtGui.QMessageBox.Cancel and saveCanceled != True:
            fileName, filters = QtGui.QFileDialog.getOpenFileName(self, "Open",
                                                         QtCore.QDir.homePath(), "MTG³ Files (*.mtg3)")
            if fileName != "":
                self.fileName = fileName
                with open(self.fileName, "r") as readfile:
                    self.centerWidget.deckTree.clear()
                    self.centerWidget.deckTree.load(json.load(readfile))
                    self.centerWidget.deckTree.selectionModel.currentRowChanged.connect(self.centerWidget.cardSelectionChanged)

    def saveFile(self):
        """Save the current file."""
        saveCanceled = False
        if self.fileName == None:
            saveCanceled = self.saveFileAs()
        else:
            with open(self.fileName, "w") as writefile:
                json.dump(self.centerWidget.deckTree.serialize(), writefile, ensure_ascii = False, indent = 4, sort_keys = True)
        return saveCanceled

    def saveFileAs(self):
        """Save the current document in a new location."""
        saveCanceled = True
        fileName, filters = QtGui.QFileDialog.getSaveFileName(self, "Save As",
                                                     QtCore.QDir.homePath(), "MTG³ Files (*.mtg3)")
        if fileName != "":
            self.fileName = fileName
            self.saveFile()
            saveCanceled = False
        return saveCanceled


    def about(self):
        """Display the about dialog."""
        self.aboutDialog = AboutDialog()
        self.aboutDialog.exec_()

    def closeEvent(self, event):
        """Handle the close event."""
        ret, saveCanceled = self.promptSave()
        if ret != QtGui.QMessageBox.Cancel and saveCanceled != True:
            event.accept()
        else:
            event.ignore()

class CenterWidget(QtGui.QWidget):

    """Manage the center widget of the main Qt window."""

    def __init__(self, parent = None):
        """Initialize the center widget."""
        super(CenterWidget, self).__init__(parent)

        self.searchEdit = SearchEdit(self)
        self.cardTree = CardTree(self)
        self.deckTree = DeckTree(self)
        self.oracleTree = OracleTree(self)
        self.cardImage = CardImage(self)
        self.cardImageThread = CardImageThread(self)

        grid = QtGui.QGridLayout()
        grid.addWidget(self.searchEdit, 0, 0)
        grid.addWidget(self.cardTree, 1, 0, 2, 1)
        grid.addWidget(self.deckTree, 0, 1, 3, 1)
        grid.addWidget(self.cardImage, 0, 2, 2, 1, QtCore.Qt.AlignHCenter)
        grid.addWidget(self.oracleTree, 2, 2)
        grid.setColumnStretch(1, 1)
        grid.setRowStretch(2, 1)
        self.setLayout(grid)

        self.searchEdit.returnPressed.connect(self.filterCards)
        self.cardTree.selectionModel.currentRowChanged.connect(self.cardSelectionChanged)
        self.deckTree.selectionModel.currentRowChanged.connect(self.cardSelectionChanged)
        self.cardImageThread.signal.imageRetrieved.connect(self.cardImageUpdate)

    def filterCards(self):
        """Filter the card list."""
        self.cardTree.clearSelection()
        self.cardTree.filterCards(self.searchEdit.text())

    def cardSelectionChanged(self, current, previous):
        """Display the oracle for the selected card."""
        global cardsDict
        if self.cardTree.hasFocus():
            key = current.sibling(current.row(), 1).data()
            if key.isdigit():
                cardIndex = int(key)
                setName = current.parent().sibling(current.parent().row(), 1).data()
                multiverseid = self.oracleTree.updateModel(setName, cardIndex)
                self.cardImageThread.run(multiverseid)
        else:
            parent = current.parent()
            if parent.data() != None:
                setName = self.deckTree.model.item(parent.row()).child(current.row(), 1).text()
                cardIndex = self.deckTree.model.item(parent.row()).child(current.row(), 2).text()
                multiverseid = self.oracleTree.updateModel(setName, int(cardIndex))
                self.cardImageThread.run(multiverseid)

    def cardImageUpdate(self, image):
        """Update the card image."""
        self.cardImage.updateImage(image)

class SearchEdit(QtGui.QLineEdit):

    """Search text box."""

    def __init__(self, parent = None):
        """Initialize the search widget."""
        super(SearchEdit, self).__init__(parent)

        self.setPlaceholderText("Type card name and press enter to search...")

class CardTree(QtGui.QTreeView):

    """Display the master card list."""

    def __init__(self, parent = None):
        """Initialize the tree widget."""
        super(CardTree, self).__init__(parent)

        self.model = CardModel(parent = self)
        self.proxyModel = CardsSortProxyModel(self)
        self.selectionModel = QtGui.QItemSelectionModel(self.proxyModel)

        self.setEditTriggers(self.NoEditTriggers)
        self.loadCardData()
        self.proxyModel.setSourceModel(self.model)
        self.setModel(self.proxyModel)
        self.setSelectionModel(self.selectionModel)
        self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.setDragEnabled(True)
        self.setDragDropMode(QtGui.QAbstractItemView.DragOnly)
        self.hideColumn(1)

    def loadCardData(self):
        """Load the master card list."""
        for k, v in cardsDict.items():
            setItem = self.model.addSet(cardsDict[k]["name"], cardsDict[k]["code"])
            for index, card in enumerate(cardsDict[k]["cards"]):
                self.model.addCard(setItem, card["name"], index)

    def filterCards(self, text):
        """Filter the master card list."""
        self.proxyModel.setFilterRegExp(QtCore.QRegExp(text, QtCore.Qt.CaseInsensitive, QtCore.QRegExp.FixedString))

    def startDrag(self, event):
        """Create a drag object containing the card information."""
        current = self.selectionModel.currentIndex()
        key = current.sibling(current.row(), 1).data()
        if key.isdigit():
            drag = QtGui.QDrag(self)
            mimeData = QtCore.QMimeData()
            cardIndex = key
            setName = current.parent().sibling(current.parent().row(), 1).data()
            cardInfo = cardIndex + "," + setName
            mimeData.setText(cardInfo)
            drag.setMimeData(mimeData)
            drag.start()

class CardModel(QtGui.QStandardItemModel):

    """Provide a model for the master card list."""

    def __init__(self, rows = 0, columns = 2, parent = None):
        """Initialize the card model."""
        super(CardModel, self).__init__(rows, columns, parent)

        self.setHeaderData(0, QtCore.Qt.Horizontal, "Cards")
        self.setHeaderData(1, QtCore.Qt.Horizontal, "Key")

    def addSet(self, name, code):
        """Add a set to the model."""
        setItem = QtGui.QStandardItem(name)
        codeItem = QtGui.QStandardItem(code)
        self.appendRow([setItem, codeItem])
        return setItem

    def addCard(self, setItem, name, index):
        """Add a card to the model."""
        nameItem = QtGui.QStandardItem(name)
        indexItem = QtGui.QStandardItem(str(index))
        setItem.appendRow([nameItem, indexItem])

class CardsSortProxyModel(QtGui.QSortFilterProxyModel):

    """Provide a sort model for the master card list."""

    def __init__(self, parent = None):
        """Initialize the card sort model."""
        super(CardsSortProxyModel, self).__init__(parent)

    def filterAcceptsRow(self, sourceRow, sourceParent):
        """Reimplement to include children in filter."""
        if super(CardsSortProxyModel, self).filterAcceptsRow(sourceRow, sourceParent):
            return True
        sourceIndex = self.sourceModel().index(sourceRow, 0, sourceParent)
        for i in range(self.sourceModel().rowCount(sourceIndex)):
            if self.filterAcceptsRow(i, sourceIndex):
                return True
        return False

class OracleTree(QtGui.QTreeView):

    """Display the card's oracle."""

    def __init__(self, parent = None):
        """Initialize the tree widget."""
        super(OracleTree, self).__init__(parent)

        self.model = OracleModel(parent = self)

        self.setEditTriggers(self.NoEditTriggers)
        self.setModel(self.model)

    def updateModel(self, set, card):
        """Update the oracle."""
        multiverseid = self.model.updateModel(set, card)
        return multiverseid

class OracleModel(QtGui.QStandardItemModel):

    """Provide a model for the master card list."""

    def __init__(self, rows = 0, columns = 2, parent = None):
        """Initialize the card model."""
        super(OracleModel, self).__init__(rows, columns, parent)

        self.setHeaderData(0, QtCore.Qt.Horizontal, "Property")
        self.setHeaderData(1, QtCore.Qt.Horizontal, "Value")

    def updateModel(self, set, card):
        """Update the oracle."""
        global cardsDict
        name = 0
        value = 1
        multiverseid = 0
        self.removeRows(0, self.rowCount())
        sortedCard = sortCard(cardsDict[set]["cards"][card])
        for row, property in enumerate(sortedCard):
            nameItem = QtGui.QStandardItem(property[name])
            valueItem = QtGui.QStandardItem(str(property[value]))
            self.setItem(row, name, nameItem)
            self.setItem(row, value, valueItem)
            toolTip = "<FONT COLOR=black>" + str(property[value]).replace("\n", "<BR />") + "</FONT>"
            self.item(row, value).setToolTip(toolTip)

            if property[name] == "Multiverse ID":
                multiverseid = property[value]
        return multiverseid

class CardImage(QtGui.QLabel):

    """Display the card image."""

    def __init__(self, parent = None):
        """Initialize the card image."""
        super(CardImage, self).__init__(parent)

        self.cardPixmap = QtGui.QPixmap("images\\card_default.jpg")

        self.setPixmap(self.cardPixmap)

    def updateImage(self, image):
        """Update the card image."""
        self.cardPixmap.loadFromData(image)
        self.setPixmap(self.cardPixmap)

class CardImageThreadSignal(QtCore.QObject):

    """Manages the card thread's signals."""

    imageRetrieved = QtCore.Signal(bytes)

class CardImageThread(QtCore.QThread):

    """Run long operations in a seperate thread."""

    def __init__(self, parent = None):
        """Initialize the thread."""
        super(CardImageThread, self).__init__(parent)
        self.signal = CardImageThreadSignal()

    def run(self, multiverseid):
        """Fetch the card image."""
        image = getCardImage(multiverseid)
        self.signal.imageRetrieved.emit(image)

class DeckTree(QtGui.QTreeView):

    """Display the deck oracle."""

    def __init__(self, parent = None):
        """Initialize the tree widget."""
        super(DeckTree, self).__init__(parent)

        self.model = DeckModel(parent = self)
        self.selectionModel = QtGui.QItemSelectionModel(self.model)
        self.undoStack = QtGui.QUndoStack(self)

        self.setEditTriggers(self.NoEditTriggers)
        self.setModel(self.model)
        self.setAcceptDrops(True)
        self.setDragDropMode(QtGui.QAbstractItemView.DropOnly)
        self.setSelectionModel(self.selectionModel)
        self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.hideColumn(1)
        self.hideColumn(2)

    def dragEnterEvent(self, event):
        """Handle the drag enter event."""
        if event.mimeData().hasFormat("text/plain"):
            event.acceptProposedAction()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        """Handle the drag move event"""
        if event.mimeData().hasFormat("text/plain"):
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """Handle a drop event."""
        global cardsDict
        cardNameColumn = 0
        cardIndex, setName = event.mimeData().text().split(",")
        cardIndex = int(cardIndex)
        cardName = cardsDict[setName]["cards"][cardIndex]["name"]
        card = cardsDict[setName]["cards"][cardIndex]
        sectionIndex, sectionName = section(card["colors"], card["types"])
        sectionItem = self.model.item(sectionIndex)
        duplicate = False
        if sectionItem.hasChildren():
            for row in range(sectionItem.rowCount()):
                existingCardName = sectionItem.child(row, cardNameColumn).text()
                if existingCardName == cardName:
                    duplicate = True
        if duplicate == False:
            command = CommandAddCard(self.model, cardIndex, setName, "Add {0}".format(cardName))
            self.undoStack.push(command)

    def deleteCard(self):
        """Delete the selected card."""
        if self.hasFocus():
            currentIndex = self.currentIndex()
            if not self.model.hasChildren(currentIndex):
                itemRow = currentIndex.row()
                sectionRow = currentIndex.parent().row()
                setName = currentIndex.sibling(itemRow, 1).data()
                cardIndex = int(currentIndex.sibling(itemRow, 2).data())
                cardName = currentIndex.data()
                command = CommandDeleteCard(self.model, cardIndex, setName, sectionRow, itemRow, "Delete {0}".format(cardName))
                self.undoStack.push(command)

    def serialize(self):
        """Serialize the deck tree."""
        global sections
        cardNameColumn, setNameColumn, indexColumn = 0, 1, 2
        deck = {}
        for k, v in sections.items():
            cards = []
            sectionItem = self.model.item(v)
            if sectionItem.hasChildren():
                for row in range(sectionItem.rowCount()):
                    cardName = sectionItem.child(row, cardNameColumn).text()
                    sectionName = sectionItem.child(row, setNameColumn).text()
                    index = sectionItem.child(row, indexColumn).text()
                    cards.append((cardName, sectionName, index))
            deck[k] = cards
        return deck

    def load(self, deck):
        """Deserialize the deck tree."""
        setNameColumn, indexColumn = 1, 2
        for k, v in sections.items():
            for card in deck[k]:
                self.model.addCard(int(card[indexColumn]), card[setNameColumn])

    def clear(self):
        """Clear the deck tree."""
        self.model = DeckModel(parent = self)
        self.selectionModel = QtGui.QItemSelectionModel(self.model)
        self.setModel(self.model)
        self.setSelectionModel(self.selectionModel)
        self.undoStack.clear()

class DeckModel(QtGui.QStandardItemModel):

    """Provide a model for the deck list."""

    def __init__(self, rows = 0, columns = 3, parent = None):
        """Initialize the card model."""
        super(DeckModel, self).__init__(rows, columns, parent)

        global sections
        self.totalCount = []
        for section in sections.items():
            self.totalCount.append(0)

        self.setHeaderData(0, QtCore.Qt.Horizontal, "Cube")
        self.setHeaderData(1, QtCore.Qt.Horizontal, "Set")
        self.setHeaderData(2, QtCore.Qt.Horizontal, "Index")

        for k, v in sections.items():
            sectionItem = QtGui.QStandardItem(k)
            self.appendRow(sectionItem)

    def addCard(self, cardIndex, setName, row = None):
        """Add a card to the model."""
        global cardsDict, sections
        card = cardsDict[setName]["cards"][cardIndex]
        sectionIndex, sectionName = section(card["colors"], card["types"])
        sectionItem = self.item(sectionIndex)
        nameItem = QtGui.QStandardItem(card["name"])
        setItem = QtGui.QStandardItem(setName)
        indexItem = QtGui.QStandardItem(str(cardIndex))
        items = [nameItem, setItem, indexItem]
        if row == None:
            sectionItem.appendRow(items)
        else:
            sectionItem.insertRow(row, items)
        sectionItem.setText(sectionName + " (" + str(sectionItem.rowCount()) + ")")
        self.totalCount[sections[sectionName]] = sectionItem.rowCount()
        self.setHeaderData(0, QtCore.Qt.Horizontal, "Cube" + " (" + str(sum(self.totalCount)) + ")")
        return sectionItem.row(), nameItem.row()

    def removeCard(self, sectionRow, itemRow):
        """Remove a card from the model."""
        global sections
        sectionItem = self.item(sectionRow)
        sectionName = sectionItem.text().split()[0]
        sectionItem.removeRow(itemRow)
        if sectionItem.rowCount() == 0:
            sectionItem.setText(sectionName)
        else:
            sectionItem.setText(sectionName + " (" + str(sectionItem.rowCount()) + ")")
        self.totalCount[sections[sectionName]] = sectionItem.rowCount()
        if sum(self.totalCount) == 0:
            self.setHeaderData(0, QtCore.Qt.Horizontal, "Cube")
        else:
            self.setHeaderData(0, QtCore.Qt.Horizontal, "Cube" + " (" + str(sum(self.totalCount)) + ")")

class CommandAddCard(QtGui.QUndoCommand):

    """Add card to deck command."""

    def __init__(self, model, cardIndex, setName, text):
        """Initialize the add undo command."""
        super(CommandAddCard, self).__init__(text)

        self.model = model
        self.cardIndex = cardIndex
        self.setName = setName

    def redo(self):
        """Add the card."""
        self.sectionRow, self.itemRow = self.model.addCard(self.cardIndex, self.setName)

    def undo(self):
        """Remove the card."""
        self.model.removeCard(self.sectionRow, self.itemRow)

class CommandDeleteCard(QtGui.QUndoCommand):

    """Delete card from deck command."""

    def __init__(self, model, cardIndex, setName, sectionRow, itemRow, text):
        """Initialize the add undo command."""
        super(CommandDeleteCard, self).__init__(text)

        self.model = model
        self.cardIndex = cardIndex
        self.setName = setName
        self.sectionRow = sectionRow
        self.itemRow = itemRow

    def redo(self):
        """Delete the card."""
        self.model.removeCard(self.sectionRow, self.itemRow)

    def undo(self):
        """Replace the card."""
        self.model.addCard(self.cardIndex, self.setName, self.itemRow)

class AboutDialog(QtGui.QDialog):

    """Display the about."""
    
    def __init__(self):
        """Initialize the dialog."""
        super(AboutDialog, self).__init__()

        iconPixmap = QtGui.QPixmap("images\\about.png")
        iconLabel = QtGui.QLabel(self)
        iconLabel.setPixmap(iconPixmap)
        creditsLabel = QtGui.QLabel("""<CENTER>
                                    Version: 1.0<BR /><BR />
                                    MTG³ was created by <A HREF=http://www.softwerks.com>Sam Beirne</A>
                                    and is distributed under the
                                    <A HREF=http://www.softwerks.com/mtg-cubed/raw/master/LICENSE.txt>MIT License.</A><BR /><BR />
                                    Magic: The Gathering is &copy; Wizards of the Coast LLC, a subsidiary of Hasbro, Inc.<BR /><BR />
                                    Icons by Mark James.<BR />
                                    <A HREF=http://www.famfamfam.com/lab/icons/silk>http://www.famfamfam.com/lab/icons/silk/</A><BR /><BR />
                                    Card data provided by Robert Shultz.<BR />
                                    <A HREF=http://mtgjson.com>http://mtgjson.com</A><BR /><BR />
                                    Source code for MTG³ is available on <A HREF=http://www.softwerks.com/mtg-cubed>Bitbucket</A>.
                                    </CENTER>""", self)
        creditsLabel.setOpenExternalLinks(True)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(iconLabel, 0, QtCore.Qt.AlignCenter)
        vbox.addWidget(creditsLabel, 1, QtCore.Qt.AlignCenter)
        vbox.addStretch(1)
        self.setLayout(vbox)

        self.setWindowTitle("About MTG³")

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    pixmap = QtGui.QPixmap("images\\splash.png")
    splash = QtGui.QSplashScreen(pixmap)
    splash.show()
    splash.showMessage("Loading card data...", QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter, QtCore.Qt.white)
    app.processEvents()
    loadCards()
    mainWindow = MainWindow()
    mainWindow.show()
    splash.finish(mainWindow)
    sys.exit(app.exec_())