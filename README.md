#MTG Cubed
MTG Cubed is a tool for building Magic: The Gathering cubes.

![Screenshot](http://i.imgur.com/SJPQhaA.png)

##External Libraries
MTG Cubed is built for [Python 3.x](http://www.python.org/) using [PySide](http://www.pyside.org).

##License
Magic: The Gathering is Copyright (c) Wizards of the Coast LLC, a subsidiary of Hasbro, Inc.

Icons by [Mark James](http://www.famfamfam.com/lab/icons/silk/).

Card data provided by [Robert Shultz](http://mtgjson.com/).

MTG Cubed is distributed under the [MIT License](https://bitbucket.org/Softwerks/mtg-cubed/raw/master/LICENSE.txt).